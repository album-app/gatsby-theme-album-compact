import os
from pathlib import Path

from album.core.controller.collection.catalog_handler import CatalogHandler
from album.core.model.catalog_index import CatalogIndex
from album.runner.core.model.coordinates import Coordinates
from album.core.model.default_values import DefaultValues
from album.core.utils.operations import file_operations


def build_resource_files():
    current_path = Path(os.path.dirname(os.path.realpath(__file__)))
    build_test_catalog("website-theme", current_path)


def build_empty_catalog(name, path):
    path.mkdir(parents=True, exist_ok=True)
    CatalogHandler.create_new_catalog(path, name)
    db_file = path.joinpath(DefaultValues.catalog_index_file_name.value)
    file_operations.force_remove(db_file)
    catalog_index = CatalogIndex(name, db_file)
    catalog_index.get_connection().commit()
    return catalog_index


def build_test_catalog(name, path):
    catalog_index = build_empty_catalog(name, path)
    catalog_index.update(Coordinates("my-group", "my-app-name", "0.1.0"), solution_attrs={
        "args": [
            {
                "default": "inp1",
                "description": "desc1",
                "name": "inp1"
            },
            {
                "default": 1.0,
                "description": "desc2",
                "name": "inp2"
            }
        ],
        "authors": ["Sample Author"],
        "cite": [{
            "text": "Citation text",
            "url": "https://citation.url",
            "doi": "Citation.DOI"
        }],
        "covers": [
            {
                "description": "cover description",
                "source": "cover.png"
            }
        ],
        "description": "keyword1",
        "documentation": [],
        "acknowledgement": "",
        "license": "MIT",
        "album_version": "0.1.0",
        "group": "my-group",
        "name": "my-app-name",
        "title": "My app",
        "version": "0.1.0",
        "tags": [
            "tag1"
        ],
        "album_api_version": "0.1.1",
        "timestamp": "2021-02-08T22:16:03.331998"
    })
    catalog_index.update(Coordinates("my-group", "my-sample-name", "0.1.0"), solution_attrs={
        "args": [
            {
                "default": "inp1",
                "description": "desc1",
                "name": "inp1"
            },
            {
                "default": 1.0,
                "description": "desc2",
                "name": "inp2"
            }
        ],
        "authors": ["Sample Author"],
        "cite": [{
            "text": "Citation text",
            "url": "https://citation.url",
            "doi": "Citation.DOI"
        }],
        "covers": [
            {
                "description": "cover description",
                "source": "cover.png"
            }
        ],
        "description": "keyword1",
        "documentation": [],
        "acknowledgement": "",
        "license": "MIT",
        "album_version": "0.1.0",
        "group": "my-group",
        "name": "my-sample-name",
        "title": "My app",
        "version": "0.1.0",
        "tags": [
            "tag1"
        ],
        "album_api_version": "0.1.1",
        "timestamp": "2021-02-08T22:16:03.331998"
    })
    catalog_index.update(Coordinates("my-group", "my-util-name", "0.1.0"), solution_attrs={
        "args": [
            {
                "default": "inp1",
                "description": "desc1",
                "name": "inp1"
            },
            {
                "default": 1.0,
                "description": "desc2",
                "name": "inp2"
            }
        ],
        "authors": ["Sample Author"],
        "cite": [{
            "text": "Citation text",
            "url": "https://citation.url",
            "doi": "Citation.DOI"
        }],
        "covers": [
            {
                "description": "cover description",
                "source": "cover.png"
            }
        ],
        "description": "keyword1",
        "documentation": [],
        "acknowledgement": "",
        "license": "MIT",
        "album_version": "0.1.0",
        "group": "my-group",
        "name": "my-util-name",
        "title": "My util solution",
        "version": "0.1.0",
        "tags": [
            "tag1"
        ],
        "album_api_version": "0.1.1",
        "timestamp": "2021-02-08T22:16:03.331998"
    })
    catalog_index.update(Coordinates("my-group", "my-solution-name", "0.1.0"), solution_attrs={
        "args": [
            {
                "default": "inp1",
                "description": "desc1",
                "name": "inp1"
            },
            {
                "default": 1.0,
                "description": "desc2",
                "name": "inp2"
            }
        ],
        "authors": ["Sample Author"],
        "cite": [{
            "text": "Citation text",
            "url": "https://citation.url",
            "doi": "Citation.DOI"
        }],
        "covers": [
            {
                "description": "cover description",
                "source": "cover.png"
            }
        ],
        "description": "keyword1",
        "documentation": [],
        "acknowledgement": "",
        "license": "MIT",
        "album_version": "0.1.0",
        "group": "my-group",
        "name": "my-solution-name",
        "title": "My solution",
        "version": "0.1.0",
        "tags": [
            "tag1"
        ],
        "album_api_version": "0.1.1",
        "timestamp": "2021-02-08T22:16:03.331998"
    })
    catalog_index.get_connection().commit()


build_resource_files()
