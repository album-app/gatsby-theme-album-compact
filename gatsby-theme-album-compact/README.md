# Gatsby Theme for album catalogs - compact version

## How to use this theme

### Prerequisites 
This theme is installed on top of an existing album catalog and makes it possible to display it's content in a browser. The catalog should contain a `/album_catalog_index.json` and `/album_catalog_index.db` file and a `/solutions/` directory containing the zipped solutions and their media files. 

### Installation
You can add the website functionality to a catalog by running the following commands (Linux):

```shell
cd PATH_TO_MY_ALBUM_CATALOG
mkdir gatsby
cd gatsby
yarn init
yarn add gatsby react react-dom gatsby-theme-album-compact
```

### Setup required files in the gatsby directory

You need to add a minimal version of your logo as PNG to `/src/images/favicon.svg`. This is used as the favicon in the browser. To change the icon on the website content header, copy your logo to `/src/gatsby-theme-album/images/logo.png`.

Next, create a markdown page in `/pages/about.md` and describe what this collection is about. Use this template:

```
---
layout: page
title: About
permalink: /about/
---

Add your about section here.
``` 

Then create a new file called `/gatsby-config.js` where you add this and adjust `title` and `subtitle` to your need: 

```javascript
module.exports = {
 siteMetadata: {
    title: 'MY ALBUM',
    subtitle: 'album catalog theme',
    menuLinks:[
      {
         name:'About',
         link:'/about'
      }
    ]
  },
  plugins: [
    {
      resolve: "gatsby-theme-album-compact",
      options: {},
    },
  ],
}
```

That's it, you can now run your gatsby site using

```shell
yarn gatsby develop
```

### Optional adjustments

#### Adding more pages
Add new pages to `/pages` analogue to `about.md` and add them to the top menu by adjusting `siteMetadata.menuLinks` in `gatsby-config.js`.