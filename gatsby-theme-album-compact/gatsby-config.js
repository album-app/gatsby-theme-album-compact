module.exports = {
 pathPrefix: '/',
 siteMetadata: {
    title: 'MY ALBUM',
    subtitle: 'album catalog theme',
    catalog_url: 'https://gitlab.com/MY_NAME/MY_CATALOG',
    menuLinks:[
      {
         name:'About',
         link:'/about'
      }
    ]
  },
  plugins: [
    `gatsby-transformer-remark`,
    `gatsby-transformer-json`,
    `gatsby-plugin-catch-links`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "GatsbyJS",
        short_name: "GatsbyJS",
        start_url: "/",
        background_color: "#f7f0eb",
        theme_color: "#a2466c",
        display: "standalone",
        icon: "src/images/favicon.png"
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `../solutions`,
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `catalog_meta`,
        path: `../album_catalog_index.json`,
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `./src/images`,
      }
    },
    {
      resolve: `gatsby-source-sqlite`,
      options: {
        fileName: `../album_catalog_index.db`,
        queries: [
          {
            statement: 'SELECT * FROM solution',
            idFieldName: 'solution_id',
            name: 'solution'
          },
          {
            statement: 'SELECT * FROM tag',
            idFieldName: 'tag_id',
            name: 'tag'
          },
          {
            statement: 'SELECT * FROM citation',
            idFieldName: 'citation_id',
            name: 'citation'
          },
          {
            statement: 'SELECT * FROM argument',
            idFieldName: 'argument_id',
            name: 'argument'
          },
          {
            statement: 'SELECT * FROM author',
            idFieldName: 'author_id',
            name: 'author'
          },
          {
            statement: 'SELECT * FROM custom',
            idFieldName: 'custom_id',
            name: 'custom'
          },
          {
            statement: 'SELECT * FROM cover',
            idFieldName: 'cover_id',
            name: 'cover',
            parentName: 'solution',
            foreignKey: 'solution_id',
            cardinality: 'OneToMany'
          },
          {
            statement: 'SELECT * FROM solution_tag',
            idFieldName: 'solution_tag_id',
            name: 'solutionTag',
          },
          {
            statement: 'SELECT * FROM solution_argument',
            idFieldName: 'solution_argument_id',
            name: 'solutionArgument',
          },
          {
            statement: 'SELECT * FROM solution_citation',
            idFieldName: 'solution_citation_id',
            name: 'solutionCitation',
          },
          {
            statement: 'SELECT * FROM solution_author',
            idFieldName: 'solution_author_id',
            name: 'solutionAuthor',
          },
          {
            statement: 'SELECT * FROM solution_custom',
            idFieldName: 'solution_custom_id',
            name: 'solutionCustom',
          }
        ]
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `pages`
      },
    },
  ],
}