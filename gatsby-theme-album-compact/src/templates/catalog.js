import React from "react"
import Layout from "../components/layout"
import SolutionList from "../components/solution-list"
import AlbumInstallGuide from "../components/album-install-guide"

const CatalogTemplate = ({ pageContext }) => {
    console.log(pageContext)
    return (
      <Layout site={pageContext.site}>
        <AlbumInstallGuide catalogUrl={pageContext.site.siteMetadata.catalog_url} />
        <SolutionList solutions={pageContext.solutions} tags={pageContext.tags} />
      </Layout>
    )
}

export default CatalogTemplate