import React from "react"
import { GatsbyImage, getImage } from "gatsby-plugin-image"

const getCover = (solution) => {
    if (solution.covers.length > 0) {
        return (<GatsbyImage className="solution-preview-image" image={getImage(solution.covers[0].img)} alt={solution.covers[0].description}/>)
    } else {
        return (<span></span>)
    }
}
const getTitle = (solution) => {
    if(solution.title) return solution.title
    else return solution.group + ":" + solution.name
}
const getDescription = (solution) => {
    if(solution.description) return (<div>{solution.description}</div>)
    else return ""
}
const getDOI = (solution) => {
    if (solution.doi) {
        console.log(solution)
        return (<><div className="solution-doi">DOI: {solution.doi != null && solution.doi != "" ? solution.doi : ""}</div></>)
    } else {
        return ("")
    }
}
const getLicense = (solution) => {
    if (solution.license) {
        return (<><div className="solution-license">License: {solution.license}</div></>)
    } else {
        return ("")
    }
}

const SolutionPreview = ({ solution }) => {
    return (
    <div className="solution-preview">
      {getCover(solution)}
      <div className="solution-preview-text">
          <div className="solution-title">{getTitle(solution)}</div>
          {getDOI(solution)}
          <div className="solution-authors">Written by {solution.solution_authors.map(author => (<React.Fragment key={author.name}><span>{author.name}</span></React.Fragment>))}</div>
          {solution.solution_citations.map(citation => (<React.Fragment key={citation.text}><div className="solution-citation">{citation.text}</div></React.Fragment>))}
          <div className="solution-tags">{solution.solution_tags.map(tag => (<React.Fragment key={tag}>{tag}</React.Fragment>))}</div>
          {getLicense(solution)}
          <div className="solution-description">{getDescription(solution)}</div>
       </div>
       <div className="solution-preview-guide">
       <code>album install {solution.group}:{solution.name}:{solution.version}</code>
       <code>album run {solution.group}:{solution.name}:{solution.version}</code>
       </div>
    </div>
)}
export default SolutionPreview