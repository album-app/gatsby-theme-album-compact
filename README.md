# Gatsby theme for album workspace

### `gatsby-theme-album-compact`

This directory is the theme package itself.

You can work on it by running
```shell
yarn workspace gatsby-theme-album-compact develop
```

